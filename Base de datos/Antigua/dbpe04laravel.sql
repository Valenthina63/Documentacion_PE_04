USE `dbpe04laravel` ;

-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Roles` (
  `idRoles` INT NOT NULL AUTO_INCREMENT,
  `nameRole` VARCHAR(45) NOT NULL,
  `conditionRole` TINYINT(1) NOT NULL,
  PRIMARY KEY (`idRoles`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Format_PE04`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Format_PE04` (
  `idFormat_PE04` INT NOT NULL AUTO_INCREMENT,
  `Code_Region` INT NOT NULL,
  `Name_Region` VARCHAR(45) NOT NULL,
  `Code_center` INT NOT NULL,
  `Name_Center` VARCHAR(45) NOT NULL,
  `Id_Tkn` INT NOT NULL,
  `Id_Unique_Tkn` INT NOT NULL,
  `State_Course` VARCHAR(45) NOT NULL,
  `Code_Level_Tra` INT NOT NULL,
  `Level_Tra` VARCHAR(45) NOT NULL,
  `Code_Day` INT NOT NULL,
  `Name_Day` VARCHAR(45) NOT NULL,
  `Type_Tra` VARCHAR(45) NOT NULL,
  `Date_Start_Tkn` DATE NOT NULL,
  `Date_Finish_Tkn` DATE NOT NULL,
  `Stage_Tkn` VARCHAR(45) NOT NULL,
  `Modality_Tra` VARCHAR(45) NOT NULL,
  `Name_Resp` VARCHAR(45) NOT NULL,
  `Id_Enterprise` INT NULL DEFAULT 0,
  `Type_Id_Enter` VARCHAR(45) NULL DEFAULT 'Ninguno',
  `Name_Enterprise` VARCHAR(45) NULL DEFAULT 'Ninguna',
  `Code_Sector_Prog` INT NOT NULL,
  `Name_Sector_Prog` VARCHAR(45) NOT NULL,
  `Code_Occupation` INT NOT NULL,
  `Name_Occupation` VARCHAR(45) NOT NULL,
  `Code_Program` INT NOT NULL,
  `Vers_Program` VARCHAR(45) NOT NULL,
  `Name_Prog_Tra` VARCHAR(45) NOT NULL,
  `Code_Country_Course` INT NOT NULL DEFAULT 57,
  `Name_Country_Course` VARCHAR(45) NOT NULL DEFAULT 'Colombia',
  `Code_Dpt_Course` INT NOT NULL,
  `Name_Dpt_Course` VARCHAR(45) NOT NULL,
  `Code_Mun_Course` INT NOT NULL,
  `Name_Mun_Course` VARCHAR(45) NOT NULL,
  `Code_Agree` INT NOT NULL DEFAULT 0,
  `Name_Agree` VARCHAR(45) NULL DEFAULT 'Ninguno',
  `Extension_Cover` VARCHAR(45) NULL DEFAULT 'Ninguno',
  `Code_Pro_Esp` INT NOT NULL,
  `Name_Pro_Esp` VARCHAR(45) NOT NULL,
  `Number_Course` INT NOT NULL,
  `Total_Appr_Male` INT NOT NULL DEFAULT 0,
  `Total_Appr_Female` INT NOT NULL DEFAULT 0,
  `Total_Appr` INT NOT NULL,
  `Hours_Plant` TIME(2) NOT NULL,
  `Hours_Contrac` TIME(2) NOT NULL,
  `Hours_Contrac_Ext` TIME(2) NOT NULL,
  `Hours_Monitors` TIME(2) NOT NULL,
  `Hours_Inst_Enter` TIME(2) NOT NULL,
  `Total_Hours` TIME(2) NOT NULL,
  `Total_Appr_Actives` INT NOT NULL,
  `Duration_Prog` INT NOT NULL DEFAULT 0,
  `Name_New_Sector` VARCHAR(45) NULL DEFAULT 'Ninguno',
  PRIMARY KEY (`idFormat_PE04`),
  UNIQUE INDEX `Id_Unique_Tkn_UNIQUE` (`Id_Unique_Tkn` ASC),
  UNIQUE INDEX `Id_Tkn_UNIQUE` (`Id_Tkn` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Roles_FormatPE04`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Roles_FormatPE04` (
  `idRoles` INT NULL,
  `idFormat_PE04` INT NULL,
  INDEX `fk_RolesFormat_Roles_idx` (`idRoles` ASC),
  INDEX `fk_RolesFormat_FormatPE04_idx` (`idFormat_PE04` ASC),
  CONSTRAINT `fk_RolesFormat_Roles`
    FOREIGN KEY (`idRoles`)
    REFERENCES `dbpe04laravel`.`Roles` (`idRoles`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_RolesFormat_FormatPE04`
    FOREIGN KEY (`idFormat_PE04`)
    REFERENCES `dbpe04laravel`.`Format_PE04` (`idFormat_PE04`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Chores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Chores` (
  `idChores` INT NOT NULL,
  `Name_ Cho` VARCHAR(45) NOT NULL,
  `Desc_Cho` TEXT(1000) NOT NULL,
  PRIMARY KEY (`idChores`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Chores_Roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Chores_Roles` (
  `IdChores` INT NULL,
  `idRoles` INT NULL,
  INDEX `fk_ChoresRoles_Chores_idx` (`IdChores` ASC),
  INDEX `fk_ChoresRoles_Roles_idx` (`idRoles` ASC),
  CONSTRAINT `fk_ChoresRoles_Chores`
    FOREIGN KEY (`IdChores`)
    REFERENCES `dbpe04laravel`.`Chores` (`idChores`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ChoresRoles_Roles`
    FOREIGN KEY (`idRoles`)
    REFERENCES `dbpe04laravel`.`Roles` (`idRoles`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Users` (
  `idUsers` INT NOT NULL,
  `Name_U` VARCHAR(45) NOT NULL,
  `LastNameU` VARCHAR(45) NOT NULL,
  `Tel_U` VARCHAR(45) NOT NULL,
  `Cell_U` VARCHAR(45) NULL DEFAULT 'No posee',
  `Mail_U` VARCHAR(45) NOT NULL,
  `User_U` VARCHAR(45) NOT NULL,
  `Pass_U` VARCHAR(45) NOT NULL,
  `Center_U` VARCHAR(45) NOT NULL,
  `idRoles` INT NOT NULL,
  `conditionU` TINYINT(1) NOT NULL,
  PRIMARY KEY (`idUsers`),
  UNIQUE INDEX `idUsers_UNIQUE` (`idUsers` ASC),
  INDEX `fk_user_roles_idx` (`idRoles` ASC),
  CONSTRAINT `fk_user_roles`
    FOREIGN KEY (`idRoles`)
    REFERENCES `dbpe04laravel`.`Roles` (`idRoles`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;