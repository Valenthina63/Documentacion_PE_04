USE `dbpe04laravel` ;

-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Roles` (
  `idRoles` INT NOT NULL AUTO_INCREMENT,
  `nameRole` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idRoles`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Homework`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Homework` (
  `id_Homework` INT NOT NULL,
  `Type_Homework` INT NULL,
  PRIMARY KEY (`id_Homework`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`HomeworkRoles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`HomeworkRoles` (
  `id_Role` INT NOT NULL,
  `id_Homework` INT NOT NULL,
  INDEX `fk_homework_idx` (`id_Homework` ASC),
  INDEX `fk_roles_idx` (`id_Role` ASC),
  CONSTRAINT `fk_homework`
    FOREIGN KEY (`id_Homework`)
    REFERENCES `dbpe04laravel`.`Homework` (`id_Homework`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_roles`
    FOREIGN KEY (`id_Role`)
    REFERENCES `dbpe04laravel`.`Roles` (`idRoles`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Regional`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Regional` (
  `cod_Regional` BIGINT NOT NULL,
  `name_Regional` VARCHAR(30) NULL,
  PRIMARY KEY (`cod_Regional`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Receipt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Receipt` (
  `id_Receipt` INT NOT NULL,
  `id_Only_Receipt` BIGINT NULL,
  `State_Course` VARCHAR(30) NULL,
  `Stage_Receipt` VARCHAR(30) NULL,
  `modality_Formation` VARCHAR(45) NULL,
  `name_Responsable` VARCHAR(45) NULL,
  PRIMARY KEY (`id_Receipt`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Formation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Formation` (
  `cod_level_Formation` BIGINT NOT NULL,
  `Level_Formation` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_level_Formation`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Word Day`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Word Day` (
  `cod_word_Day` BIGINT NOT NULL,
  `name_Word_Day` VARCHAR(45) NULL,
  `type_Formation` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_word_Day`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Occupation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Occupation` (
  `cod_Occupation` BIGINT NOT NULL,
  `name_Occupation` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_Occupation`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`DateTime`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`DateTime` (
  `id_Date` BIGINT NOT NULL,
  `date_Starr_Receipt` DATE NULL,
  `date_Termination_Receipt` DATE NULL,
  `hours_Plant` TIME NULL,
  `hours_Contractor` TIME NULL,
  `hours_Contractor_External` TIME NULL,
  `hours_Monitor` TIME NULL,
  `hours_Inst_Company` TIME NULL,
  `toatal_Hours` TIME NULL,
  `duration_Program` BIGINT NULL,
  PRIMARY KEY (`id_Date`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Program`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Program` (
  `cod_Program` BIGINT NOT NULL,
  `version_Program` BIGINT NULL,
  `name_Program_Formation` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_Program`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Country Course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Country Course` (
  `cod_Country_Course` BIGINT NOT NULL,
  `name_Country_Course` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_Country_Course`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Center`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Center` (
  `cod_Center` BIGINT NOT NULL,
  `name_Center` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_Center`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Course` (
  `number_Course` BIGINT NOT NULL,
  `tatal_Apprentices_Masculine` BIGINT NULL,
  `total_Apprentices_Feminine` BIGINT NULL,
  `tatal_Apprentices` BIGINT NULL,
  `total_Apprentices_Assets` BIGINT NULL,
  PRIMARY KEY (`number_Course`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Company`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Company` (
  `id_Company` BIGINT NOT NULL,
  `name_Company` VARCHAR(45) NULL,
  `type_id_Company` BIGINT NULL,
  PRIMARY KEY (`id_Company`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Municipaly Course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Municipaly Course` (
  `cod_Municipaly_Course` BIGINT NOT NULL,
  `name_Municipaly_Course` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_Municipaly_Course`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`SectorProgram`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`SectorProgram` (
  `cod_Sector_Program` BIGINT NOT NULL,
  `name_Sector_Program` VARCHAR(45) NULL,
  `name_New_Sector` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_Sector_Program`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`ProgramSpecial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`ProgramSpecial` (
  `cod_Program_Special` BIGINT NOT NULL,
  `name_Program_Special` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_Program_Special`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Convenio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Convenio` (
  `cod_Agreement` BIGINT NOT NULL,
  `name_Agreement` VARCHAR(45) NULL,
  `extencion_Covarage` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_Agreement`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`DepartmentCourse`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`DepartmentCourse` (
  `cod_Department_Course` BIGINT NOT NULL,
  `name_Department_Course` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_Department_Course`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`FormatPE04`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`FormatPE04` (
  `id_Format` BIGINT NOT NULL,
  `cod_Regional` BIGINT NOT NULL,
  `cod_Center` BIGINT NOT NULL,
  `cod_level_Formation` BIGINT NOT NULL,
  `cod_word_Day` BIGINT NOT NULL,
  `id_Date` BIGINT NOT NULL,
  `id_Company` BIGINT NOT NULL,
  `cod_SEctor_Program` BIGINT NOT NULL,
  `cod_Occupation` BIGINT NOT NULL,
  `cod_Program` BIGINT NOT NULL,
  `cod_Country_Course` BIGINT NOT NULL,
  `cod_Department_Course` BIGINT NOT NULL,
  `cod_Municipaly_Course` BIGINT NOT NULL,
  `cod_Agreement` BIGINT NOT NULL,
  `cod_Program_Special` BIGINT NOT NULL,
  `number_Course` BIGINT NOT NULL,
  `id_Receipt` INT NOT NULL,
  PRIMARY KEY (`id_Format`),
  INDEX `fk_FormatPE04_Regional1_idx` (`cod_Regional` ASC),
  INDEX `fk_FormatPE04_Formation1_idx` (`cod_level_Formation` ASC),
  INDEX `fk_FormatPE04_Word Day1_idx` (`cod_word_Day` ASC),
  INDEX `fk_FormatPE04_Occupation1_idx` (`cod_Occupation` ASC),
  INDEX `fk_FormatPE04_DateTime1_idx` (`id_Date` ASC),
  INDEX `fk_FormatPE04_Country Course1_idx` (`cod_Country_Course` ASC),
  INDEX `fk_FormatPE04_Center1_idx` (`cod_Center` ASC),
  INDEX `fk_FormatPE04_Copurse1_idx` (`number_Course` ASC),
  INDEX `fk_FormatPE04_Company1_idx` (`id_Company` ASC),
  INDEX `fk_FormatPE04_MunicipalyCourse1_idx` (`cod_Municipaly_Course` ASC),
  INDEX `fk_FormatPE04_SectorProgram1_idx` (`cod_SEctor_Program` ASC),
  INDEX `fk_FormatPE04_Convenio1_idx` (`cod_Agreement` ASC),
  INDEX `fk_FormatPE04_DepartmentCourse1_idx` (`cod_Department_Course` ASC),
  INDEX `fk_FormatPE04_Program1_idx` (`cod_Program` ASC),
  INDEX `fk_FormatPE04_ProgramSpecial1_idx` (`cod_Program_Special` ASC),
  INDEX `fk_FormatPE04_Receipt1_idx` (`id_Receipt` ASC),
  CONSTRAINT `fk_FormatPE04_Regional1`
    FOREIGN KEY (`cod_Regional`)
    REFERENCES `dbpe04laravel`.`Regional` (`cod_Regional`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_Receipt1`
    FOREIGN KEY (`id_Receipt`)
    REFERENCES `dbpe04laravel`.`Receipt` (`id_Receipt`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_Formation1`
    FOREIGN KEY (`cod_level_Formation`)
    REFERENCES `dbpe04laravel`.`Formation` (`cod_level_Formation`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_Word Day1`
    FOREIGN KEY (`cod_word_Day`)
    REFERENCES `dbpe04laravel`.`Word Day` (`cod_word_Day`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_Occupation1`
    FOREIGN KEY (`cod_Occupation`)
    REFERENCES `dbpe04laravel`.`Occupation` (`cod_Occupation`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_DateTime1`
    FOREIGN KEY (`id_Date`)
    REFERENCES `dbpe04laravel`.`DateTime` (`id_Date`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_Program1`
    FOREIGN KEY (`cod_Program`)
    REFERENCES `dbpe04laravel`.`Program` (`cod_Program`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_Country Course1`
    FOREIGN KEY (`cod_Country_Course`)
    REFERENCES `dbpe04laravel`.`Country Course` (`cod_Country_Course`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_Center1`
    FOREIGN KEY (`cod_Center`)
    REFERENCES `dbpe04laravel`.`Center` (`cod_Center`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_Course1`
    FOREIGN KEY (`number_Course`)
    REFERENCES `dbpe04laravel`.`Course` (`number_Course`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_Company1`
    FOREIGN KEY (`id_Company`)
    REFERENCES `dbpe04laravel`.`Company` (`id_Company`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_MunicipalyCourse1`
    FOREIGN KEY (`cod_Municipaly_Course`)
    REFERENCES `dbpe04laravel`.`Municipaly Course` (`cod_Municipaly_Course`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_SectorProgram1`
    FOREIGN KEY (`cod_SEctor_Program`)
    REFERENCES `dbpe04laravel`.`SectorProgram` (`cod_Sector_Program`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_ProgramSpecial1`
    FOREIGN KEY (`cod_Program_Special`)
    REFERENCES `dbpe04laravel`.`ProgramSpecial` (`cod_Program_Special`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_Convenio1`
    FOREIGN KEY (`cod_Agreement`)
    REFERENCES `dbpe04laravel`.`Convenio` (`cod_Agreement`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FormatPE04_DepartmentCourse1`
    FOREIGN KEY (`cod_Department_Course`)
    REFERENCES `dbpe04laravel`.`DepartmentCourse` (`cod_Department_Course`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Users` (
  `idUsers` BIGINT NOT NULL,
  `Name_U` VARCHAR(45) NOT NULL,
  `LastNameU` VARCHAR(45) NOT NULL,
  `Roles_idRoles` INT NOT NULL,
  PRIMARY KEY (`idUsers`),
  UNIQUE INDEX `idUsers_UNIQUE` (`idUsers` ASC),
  INDEX `fk_Users_Roles1_idx` (`Roles_idRoles` ASC),
  CONSTRAINT `fk_Users_Roles1`
    FOREIGN KEY (`Roles_idRoles`)
    REFERENCES `dbpe04laravel`.`Roles` (`idRoles`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`UsersFormatPE04`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`UsersFormatPE04` (
  `id_User` BIGINT NOT NULL,
  `id_Format` BIGINT NOT NULL,
  INDEX `fk_UsersFormatPE04_FormatPE041_idx` (`id_Format` ASC),
  CONSTRAINT `fk_UsersFormatPE04_FormatPE041`
    FOREIGN KEY (`id_Format`)
    REFERENCES `dbpe04laravel`.`FormatPE04` (`id_Format`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UsersFormatPE04_Users1`
    FOREIGN KEY (`id_User`)
    REFERENCES `dbpe04laravel`.`Users` (`idUsers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Format_PE04`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Format_PE04` (
  `idFormat_PE04` INT NOT NULL AUTO_INCREMENT,
  `Code_Region` INT NOT NULL,
  `Name_Region` VARCHAR(45) NOT NULL,
  `Code_center` INT NOT NULL,
  `Name_Center` VARCHAR(45) NOT NULL,
  `Id_Tkn` INT NOT NULL,
  `Id_Unique_Tkn` INT NOT NULL,
  `State_Course` VARCHAR(45) NOT NULL,
  `Code_Level_Tra` INT NOT NULL,
  `Level_Tra` VARCHAR(45) NOT NULL,
  `Code_Day` INT NOT NULL,
  `Name_Day` VARCHAR(45) NOT NULL,
  `Type_Tra` VARCHAR(45) NOT NULL,
  `Date_Start_Tkn` DATE NOT NULL,
  `Date_Finish_Tkn` DATE NOT NULL,
  `Stage_Tkn` VARCHAR(45) NOT NULL,
  `Modality_Tra` VARCHAR(45) NOT NULL,
  `Name_Resp` VARCHAR(45) NOT NULL,
  `Id_Enterprise` INT NULL DEFAULT 0,
  `Type_Id_Enter` VARCHAR(45) NULL DEFAULT 'Ninguno',
  `Name_Enterprise` VARCHAR(45) NULL DEFAULT 'Ninguna',
  `Code_Sector_Prog` INT NOT NULL,
  `Name_Sector_Prog` VARCHAR(45) NOT NULL,
  `Code_Occupation` INT NOT NULL,
  `Name_Occupation` VARCHAR(45) NOT NULL,
  `Code_Program` INT NOT NULL,
  `Vers_Program` VARCHAR(45) NOT NULL,
  `Name_Prog_Tra` VARCHAR(45) NOT NULL,
  `Code_Country_Course` INT NOT NULL DEFAULT 57,
  `Name_Country_Course` VARCHAR(45) NOT NULL DEFAULT 'Colombia',
  `Code_Dpt_Course` INT NOT NULL,
  `Name_Dpt_Course` VARCHAR(45) NOT NULL,
  `Code_Mun_Course` INT NOT NULL,
  `Name_Mun_Course` VARCHAR(45) NOT NULL,
  `Code_Agree` INT NOT NULL DEFAULT 0,
  `Name_Agree` VARCHAR(45) NULL DEFAULT 'Ninguno',
  `Extension_Cover` VARCHAR(45) NULL DEFAULT 'Ninguno',
  `Code_Pro_Esp` INT NOT NULL,
  `Name_Pro_Esp` VARCHAR(45) NOT NULL,
  `Number_Course` INT NOT NULL,
  `Total_Appr_Male` INT NOT NULL DEFAULT 0,
  `Total_Appr_Female` INT NOT NULL DEFAULT 0,
  `Total_Appr` INT NOT NULL,
  `Hours_Plant` TIME NOT NULL,
  `Hours_Contrac` TIME NOT NULL,
  `Hours_Contrac_Ext` TIME NOT NULL,
  `Hours_Monitors` TIME NOT NULL,
  `Hours_Inst_Enter` TIME NOT NULL,
  `Total_Hours` TIME NOT NULL,
  `Total_Appr_Actives` INT NOT NULL,
  `Duration_Prog` INT NOT NULL DEFAULT 0,
  `Name_New_Sector` VARCHAR(45) NULL DEFAULT 'Ninguno',
  PRIMARY KEY (`idFormat_PE04`),
  UNIQUE INDEX `Id_Unique_Tkn_UNIQUE` (`Id_Unique_Tkn` ASC),
  UNIQUE INDEX `Id_Tkn_UNIQUE` (`Id_Tkn` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Roles_FormatPE04`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Roles_FormatPE04` (
  `idRoles` INT NULL DEFAULT NULL,
  `idFormat_PE04` INT NULL DEFAULT NULL,
  INDEX `fk_RolesFormat_Roles_idx` (`idRoles` ASC),
  INDEX `fk_RolesFormat_FormatPE04_idx` (`idFormat_PE04` ASC),
  CONSTRAINT `fk_RolesFormat_Roles`
    FOREIGN KEY (`idRoles`)
    REFERENCES `dbpe04laravel`.`Roles` (`idRoles`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_RolesFormat_FormatPE04`
    FOREIGN KEY (`idFormat_PE04`)
    REFERENCES `dbpe04laravel`.`Format_PE04` (`idFormat_PE04`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Chores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Chores` (
  `idChores` INT NOT NULL,
  `Name_ Cho` VARCHAR(45) NOT NULL,
  `Desc_Cho` TEXT(1000) NOT NULL,
  PRIMARY KEY (`idChores`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbpe04laravel`.`Chores_Roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbpe04laravel`.`Chores_Roles` (
  `IdChores` INT NULL DEFAULT NULL,
  `idRoles` INT NULL DEFAULT NULL,
  INDEX `fk_ChoresRoles_Chores_idx` (`IdChores` ASC),
  INDEX `fk_ChoresRoles_Roles_idx` (`idRoles` ASC),
  CONSTRAINT `fk_ChoresRoles_Chores`
    FOREIGN KEY (`IdChores`)
    REFERENCES `dbpe04laravel`.`Chores` (`idChores`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ChoresRoles_Roles`
    FOREIGN KEY (`idRoles`)
    REFERENCES `dbpe04laravel`.`Roles` (`idRoles`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
