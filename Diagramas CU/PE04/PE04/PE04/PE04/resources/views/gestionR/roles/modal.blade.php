<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$cat->idRoles}}" >
		{{Form::Open(array('action'=>array('RolesController@destroy',$cat->idRoles),'method'=>'delete'))}}
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" adia-label="Close">
						<samp aria-hidden="true">x</samp>
					</button>
					<h4 class="modal-title">Eliminar Rol</h4>
				</div>
				<div class="modal-body">
					<p>Confirme Si Desea Eliminar El Rol</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Confirmar</button>
				</div>
			</div>
		</div>

		{{Form::Close()}}
</div>