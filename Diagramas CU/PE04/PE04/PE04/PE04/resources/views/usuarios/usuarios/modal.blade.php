<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$usu->idUsers}}" >
		{{Form::Open(array('action'=>array('usuarioController@destroy',$usu->idUsers),'method'=>'delete'))}}
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" adia-label="Close">
						<samp aria-hidden="true">x</samp>
					</button>
					<h4 class="modal-title">Eliminar usuario</h4>
				</div>
				<div class="modal-body">
					<p>Confirme Si Desea Eliminar El Usuario</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Confirmar</button>
				</div>
			</div>
		</div>

		{{Form::Close()}}
</div>