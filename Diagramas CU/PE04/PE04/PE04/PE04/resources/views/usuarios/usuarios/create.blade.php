@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Nuevo usuario</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif

			{!!Form::open(array('url'=>'usuarios/usuarios','method'=>'POST','autocomplete'=>'off'))!!}
			{{Form::token()}}
			<div class="form-group">
				<label for="idUsers">id usuario</label>
				<input type="number" name="idUsers" class="form-control" placeholder="id usuario">
			</div>
			<div class="form-group">
				<label for="Name_U">Nombre:</label>
				<input type="text" name="Name_U" class="form-control" placeholder="Nombre">
			</div>
			
			<div class="form-group">
				<label for="LastNameU">Apellido:</label>
				<input type="text" name="LastNameU" class="form-control" placeholder="Apellido">
			</div>
			<div class="form-group">
				<label for="Tel_U">Telefono:</label>
				<input type="number" name="Tel_U" class="form-control" placeholder="Telefono">
			</div>


			<div class="form-group">
				<button class="btn btn-primary" type="submit">Guardar</button>
				
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
			{!!Form::close()!!}
		</div>
	</div>
@endsection