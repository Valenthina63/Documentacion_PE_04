@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar usuario: {{$usuarios-> Name_U}}</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif

			{!!Form::model($usuarios,['method'=>'PATCH','route'=>['usuarios.usuarios.update',$usuarios->idUsers]])!!}
			{{Form::token()}}

			<div class="form-group">
				<label for="usuario">id usuario:</label>
				<input type="number" name="idUsers" class="form-control" value="{{$usuarios->idUsers}}" placeholder="id usuario">
			</div>
			<div class="form-group">
				<label for="usuario">Nombre:</label>
				<input type="text" name="Name_U" class="form-control" value="{{$usuarios->Name_U}}" placeholder="nombre usuario">
			</div>
			<div class="form-group">
				<label for="usuario">Apellido:</label>
				<input type="text" name="LastNameU" class="form-control" value="{{$usuarios->LastNameU}}"
				placeholder="Apellido usuario">
			</div>
			<div class="form-group">
				<label for="usuario">Telefono:</label>
				<input type="text" name="Tel_U" class="form-control" value="{{$usuarios->Tel_U}}" placeholder="Telefono">
			</div>
			<div class="form-group">
				<label for="usuario">Celular:</label>
				<input type="number" name="Cell_U" class="form-control" value="{{$usuarios->Cell_U}}" placeholder="Celular">
			</div>
			<div class="form-group">
				<label for="usuario">Correo:</label>
				<input type="email" name="Mail_U" class="form-control" value="{{$usuarios->Mail_U}}" placeholder="Correo">
			</div>
			<div class="form-group">
				<label for="usuario">Nombre usuario:</label>
				<input type="text" name="User_U" class="form-control" value="{{$usuarios->User_U}}" placeholder="nomb usuario">
			</div>
			<div class="form-group">
				<label for="usuario">Contraseña:</label>
				<input type="password" name="Pass_U" class="form-control" value="{{$usuarios->Pass_U}}" placeholder="Contraseña">
			</div>
			

			<div class="form-group">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>

			<div class="form-group">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
			{!!Form::close()!!}
		</div>
	</div>

@endsection