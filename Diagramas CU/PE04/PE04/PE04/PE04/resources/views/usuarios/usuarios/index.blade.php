@extends('layouts.admin')
@section('contenido')
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<h3>Nuevo usuario: <a href="usuarios/create"> <button class="btn btn-success">NUEVO</button></a></h3>	
			@include('usuarios.usuarios.search')
		</div>	
	</div>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="table-responsive">
				<table class="table table-striped table-bordered table-condensed table-hover">
					<thead>
						<th>idUsers</th>
						<th>Name_U</th>
						<th>LastNameU</th>
						<th>Tel_U</th>
						<th>Cell_U</th>
						<th>Mail_U</th>
						<th>User_U</th>
						<th>Opciones</th>
					</thead>
					@foreach ($usuarios as $usu)
					<tr>
						<td>{{ $usu-> idUsers}}</td>
						<td>{{ $usu->Name_U}} </td>
						<td>{{ $usu-> LastNameU}} </td>
						<td>{{ $usu-> Tel_U }}</td>
						<td>{{ $usu-> Cell_U}} </td>
						<td>{{ $usu-> Mail_U}} </td>
						<td>{{ $usu-> User_U}} </td>
						<td>{{ $usu-> Pass_U}} </td>
						<td>
						 	<a href="{{URL::action('usuarioController@edit',$usu-> idUsers)}}"><button class="btn btn-info">Editar</button></a>
						 	<a href="" data-target="#modal-delete-{{$usu->idUsers}}" data-toggle="modal"><button class="btn btn-danger" >Eliminar</button></a>
						 </td>
						 </tr>	
					@include('usuarios.usuarios.modal')			
					@endforeach
				</table>
					
				</div>
				{{$usuarios->render()}}

			</div>

		</div>








@endsection