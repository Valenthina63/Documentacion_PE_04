<?php

namespace pe04;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table='roles';

    protected $primaryKey='idRoles';

    public $timestamps=false;

    protected $filleable =[
    	'nameRole',
    	'conditionRole'
    ];

    protected $guarded=[

    ];
}
