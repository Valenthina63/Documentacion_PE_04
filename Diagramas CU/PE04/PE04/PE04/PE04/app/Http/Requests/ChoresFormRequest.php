<?php

namespace pe04\Http\Requests;

use pe04\Http\Requests\Request;

class ChoresFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idChores' => 'required',
            'Name_Cho' => 'required|max:45',
            'Desc_Cho' => 'required|max:512'
        ];
    }
}
