<?php

namespace pe04\Http\Requests;

use pe04\Http\Requests\Request;

class usuarioFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'Name_U'=>'required|max:100',
        'LastNameU'=>'required|max:100',
        'Tel_U'=>'required|max:20',
        'Cell_U'=>'required|max:15',
        'Mail_U'=>'required|max:50',
        'User_U'=>'required|max:30',
        'Pass_U'=>'required|max:100',
        'Center_U'=>'required|max:100',
        'idRoles'=>'required|max:100',
        'conditionU'=>'required|max:100'
        ];
    }
}
