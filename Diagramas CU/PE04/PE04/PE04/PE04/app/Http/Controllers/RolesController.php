<?php

namespace pe04\Http\Controllers;

use Illuminate\Http\Request;

use pe04\Http\Requests;
use pe04\Roles;
use Illuminate\support\facades\redirect;
use pe04\Http\Requests\RolesFormRequest;
use DB;

class RolesController extends Controller
{
    public function __construct()
    {

    }
    public function index(Request $request)
    {
    	if($request)
    	{
    		$query=trim($request->get('searchText'));
    		$roles=DB::table('roles')->where('nameRole','LIKE','%'.$query.'%')
    		->where('conditionRole','=','1')
    		->orderBy('idRoles','desc')
    		->paginate(7);
    		return view('gestionR.roles.index',["roles"=>$roles,"searchText"=>$query]);
    	}

    }
    public function create()
    {
    	return view("gestionR.roles.create");
    }
    public function store(RolesFormRequest $request)
    {
    	$roles=new Roles;
    	$roles->nameRole=$request->get('nameRole');
    	$roles->conditionRole='1';
    	$roles->save();
    	return Redirect::to('gestionR/roles');
    }
    public function show($id)
    {
    	return view('gestionR.roles.show',["roles"=>Roles::findOrFail($id)]);
    }
    public function edit($id)
    {
    	return view('gestionR.roles.edit',["roles"=>Roles::findOrFail($id)]);
    }
    public function update(RolesFormRequest $request,$id)
    {
    	$roles=Roles::findOrFail($id);
    	$roles->nameRole=$request->get('nameRole');
        $roles->update();
    	return Redirect::to('gestionR/roles');
    }
    public function destroy($id)
    {
    	$roles=Roles::findOrFail($id);
    	$roles->conditionRole='0';
    	$roles->update();
    	return Redirect::to('gestionR/roles');    	
    }
}
