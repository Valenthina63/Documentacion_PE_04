<?php

namespace pe04\Http\Controllers;

use Illuminate\Http\Request;
use pe04\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use pe04\Http\Requests\ChoresFormRequest;
use pe04\Chores;
use DB;

class ChoresController extends Controller
{
     public function __construct()
    {

    }
    public function index(Request $request)
    {
    	if($request)
    	{
    		$query=trim($request->get('searchText'));
    		$chores=DB::table('chores')
    		->where('Name_Cho','LIKE','%'.$query.'%')
     		->orderBy('idChores','desc')
    		->paginate(7);
    		return view('gestionR.chores.index',["chores"=>$chores,"searchText"=>$query]);
    	}

    }
    public function create()
    {
    	return view("gestionR.chores.create");
    }
    public function store(ChoresFormRequest $request)
    {
    	$chores=new Chores;
    	$chores->idChores=$request->get('idChores');
    	$chores->Name_Cho=$request->get('Name_Cho');
    	$chores->Desc_Cho=$request->get('Desc_Cho');
      	$chores->save();
    	return Redirect::to('gestionR/chores');
    }
    public function show($id)
    {
    	return view('gestionR.chores.show',["chores"=>Chores::findOrFail($id)]);
    }
    public function edit($id)
    {
    	$chores=Chores::findOrFail($id);
    	$roles=DB::table('roles')->where('condicion','=','1')->get();
    	return view('gestionR.chores.edit',["chores"=>$chores,"roles"=>$roles]);
    }
    public function update(ChoresFormRequest $request,$id)
    {
    	$chores=Chores::findOrFail($id);
    	$chores->idChores=$request->get('idChores');
    	$chores->Name_Cho=$request->get('Name_Cho');
    	$chores->Desc_Cho=$request->get('Desc_Cho');
        $chores->update();
    	return Redirect::to('gestionR/chores');
    }
    public function destroy($id)
    {
    	$chores=Chores::findOrFail($id);
    	$chores->Desc_Cho='0';
    	$chores->update();
    	return Redirect::to('gestionR/chores');    	
    }
}
