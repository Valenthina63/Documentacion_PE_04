<?php

namespace pe04\Http\Controllers;

use Illuminate\Http\Request;

use pe04\Http\Requests;
use pe04\usuario;
use Illuminate\Support\Facades\Redirect;
use pe04\http\Requests\usuarioFormRequest;
use DB;

class usuarioController extends Controller
{
    public function __construct()
    {

    }
    public function index(Request $request)
    {
    	if($request)
    	{
    		$query=trim($request->get('searchText'));
    		$usuario=DB::table('Users')
    		->where('Name_U','LIKE','%'.$query.'%')
    		->where('LastNameU','=','usuario')
    		->where('idUsers','LIKE','%'.$query.'%')
    		->where('LastNameU','=','usuarios')
    		->orderBy('idUsers','desc')
    		->paginate(7);

    		return view('usuarios.usuarios.index',["usuarios"=>$usuario,"searchText"=>$query]);
    	}

    }
    public function create()
    {
    	return view("usuarios.usuarios.create");
    }
    public function store(usuarioFormRequest $request)
    {
    	$usuario=new usuario;

    	$usuario->Name_U=$request->get('Name_U');
    	$usuario->LastNameU=$request->get('LastNameU');
    	$usuario->Tel_U=$request->get('Tel_U');
    	$usuario->Cell_U=$request->get('Cell_U');
    	$usuario->Mail_U=$request->get('Mail_U');
    	$usuario->User_U=$request->get('User_U');
    	$usuario->Pass_U=$request->get('Pass_U');
    	$usuario->Center_U=$request->get('Center_U');
    	$usuario->idRoles='Roles';


    	$usuario->save();
    	return Redirect::to('usuarios/usuarios');
    }
    public function show($idUsers)
    {
    	return view('usuarios.usuarios.show',["usuarios"=>usuario::findOrFail($idUsers)]);
    }
    public function edit($idUsers)
    {
    	return view('usuarios.usuarios.edit',["usuarios"=>usuario::findOrFail($idUsers)]);
    }
    public function update(usuarioFormRequest $request,$idUsers)
    {
    	$usuario=usuario::findOrFail($idUsers);

    	$usuario->Name_U=$request->get('Name_U');
    	$usuario->LastNameU=$request->get('LastNameU');
    	$usuario->Tel_U=$request->get('Tel_U');
    	$usuario->Cell_U=$request->get('Cell_U');
    	$usuario->Mail_U=$request->get('Mail_U');
    	$usuario->User_U=$request->get('User_U');
    	$usuario->Pass_U=$request->get('Pass_U');
    	$usuario->Center_U=$request->get('Center_U');
    	$usuario->idRoles='Roles';

        $usuario->update();
    	return Redirect::to('usuarios/usuarios');
    }
    public function destroy($idUsers)
    {
    	$usuario=usuario::findOrFail($id);

        $usuario->Name_U;
        $usuario->LastNameU;
        $usuario->Tel_U;
        $usuario->Cell_U;
        $usuario->Mail_U;
        $usuario->User_U;
        $usuario->Pass_U;
        $usuario->Center;
        $usuario->idRoles;

    	$usuario->update();
    	return Redirect::to('usuarios/usuarios');    	
    }
}

