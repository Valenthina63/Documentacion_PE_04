<?php

namespace pe04;

use Illuminate\Database\Eloquent\Model;

class Chores extends Model
{
    protected $table='chores';

    protected $primaryKey='idChores';

    public $timestamps=false;

    protected $filleable =[
    	'idChores',
    	'Name_Cho',
    	'Desc_Cho'
    ];

    protected $guarded=[

    ];
}
