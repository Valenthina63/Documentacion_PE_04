<?php

namespace pe04;

use Illuminate\Database\Eloquent\Model;

class usuario extends Model
{
   protected $table='usuario';

    protected $primaryKey='idUsers';

    public $timestamps=false;

    protected $filleable =[
    	'Name_U',
    	'LastNameU',
    	'Tel_U',
    	'Cell_U',
    	'Mail_U',
    	'User_U',
    	'Pass_U',
    	'Center_U',
    	'idRoles',
    	'conditionU'
    ];

    protected $guarded=[

    ];
}
